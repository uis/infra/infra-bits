package Ucam::Infra::DominionPX;

use warnings;
use strict;

use base qw(Ucam::Infra::PDU);

use SNMP;

sub probe {
    my $class = shift;
    my($sysoid) = @_;
    return $sysoid eq '.1.3.6.1.4.1.13742.4';
}

sub construct {
    my $proto = shift;
    SNMP::loadModules('PDU-MIB');
    my $self = $proto->SUPER::construct(@_);
    my $ver = $self->snmp->get("firmwareVersion.0");
    defined $ver or
	die("SNMP request failed: ", $self->snmp->{ErrorStr}, "\n");
    $ver eq '01.01.00' or $ver ge '01.02.07' or
	die "$self: unknown DPX firmware version: $ver\n";
    $self->{'Ucam::Infra::DominionPX::ver'} = $ver;
    return $self;
}

sub mapoutlet {
    my ($self, $outlet) = @_;
    $outlet-- if $self->{'Ucam::Infra::DominionPX::ver'} eq '01.01.00';
    return $outlet;
}

sub getstatus {
    my ($self, $outlet) = @_;
    $outlet = $self->mapoutlet($outlet);
    my $val = $self->snmp->get("outletOperationalState.$outlet");
    my $current = $self->snmp->get("outletCurrent.$outlet") / 1000;
    return sprintf("%s (%.2f A)", $val, $current);
}    

sub getvers {
    my ($self) = @_;
    return $self->{'Ucam::Infra::DominionPX::ver'};
}

sub getmodel {
    my ($self) = @_;
    return $self->snmp->get("objectName.0");
}

sub power_state {
    my ($self, $outlet) = @_;
    $outlet = $self->mapoutlet($outlet);
    my $val = $self->snmp->get("outletOperationalState.$outlet");
    return undef unless defined $val;
    return $val eq 'on';
}

sub on {
    my ($self, $outlet) = @_;
    $outlet = $self->mapoutlet($outlet);
    $self->snmp->set("outletOperationalState.$outlet", 'on');
    if ($self->snmp->{ErrorNum}) {
	warn("SNMP request failed: ", $self->snmp->{ErrorStr}, "\n");
    }
}

sub off {
    my ($self, $outlet) = @_;
    $outlet = $self->mapoutlet($outlet);
    $self->snmp->set("outletOperationalState.$outlet", 'off');
    if ($self->snmp->{ErrorNum}) {
	warn("SNMP request failed: ", $self->snmp->{ErrorStr}, "\n");
    }
}

sub reboot {
    my ($self, $outlet) = @_;
    $outlet = $self->mapoutlet($outlet);
    $self->snmp->set("outletOperationalState.$outlet", 'cycling');
    if ($self->snmp->{ErrorNum}) {
	warn("SNMP request failed: ", $self->snmp->{ErrorStr}, "\n");
    }
}

sub outletname {
    my ($self, $outlet, $name) = @_;
    my $outletidx = $self->mapoutlet($outlet);
    if (defined $name) {
	# Dominion PX 01.05.07 won't accept labels over 32 characters long.
        $name = substr($name, 0, 32);
	if ($self->snmp->get("outletLabel.$outletidx") ne $name) {
	    $self->snmp->set("outletLabel.$outletidx", $name);
	}
	my $setname = $self->snmp->get("outletLabel.$outletidx");
	if ($setname ne $name) {
	    warn("$self:$outlet: tried to change label to '$name'".
		 " but got '$setname' instead\n");
	}
    } else {
	$name = $self->snmp->get("outletLabel.$outlet");
    }
    return $name
}

sub getcurrent {
    my($self, $outlet) = @_;
    my $outletidx = $self->mapoutlet($outlet);
    my $milliampere;
    if (defined $outlet) {
	$milliampere = $self->snmp->get("outletCurrent.$outletidx");
    } else {
	$milliampere = $self->snmp->get("unitCurrent.0");
    }
    return $milliampere / 1000;
}

sub getoutletcurrent {
    my($self, $outlet) = @_;
    my $outletidx = $self->mapoutlet($outlet);
    my $milliampere;
    $milliampere = $self->snmp->get("outletCurrent.$outletidx");
    return $milliampere / 1000;
}

sub getsensorvalue {
    my($self, $sensor) = @_;
    my $value = $self->snmp->get("externalSensorValue.$sensor");
    my $shift = $self->snmp->get("externalSensorDecimalDigits.$sensor");
    return $value / 10**$shift;
}

1;
