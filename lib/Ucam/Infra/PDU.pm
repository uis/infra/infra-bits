package Ucam::Infra::PDU;

use warnings;
use strict;

use base 'Ucam::Infra::DBI';

use SNMP;
use Ucam::Infra::PDU::Any;

__PACKAGE__->table('pdus');
__PACKAGE__->columns(Essential => qw/pduname/);
__PACKAGE__->has_many(outlets => 'Ucam::Infra::PDU::Outlet', 'pduname');

my $rrddir = "/var/lib/infra-stats";

sub new {
    my $proto = shift;

    return Ucam::Infra::PDU::Any->retrieve(@_);
}

sub construct {
    my $proto = shift;
    
    my $self = $proto->SUPER::construct(@_);
    my $pduname = $self->pduname;
    my $snmp = new SNMP::Session(DestHost => "$pduname.infra", Version => 1,
				 Community => "private", UseEnums => 1)
	or die "Can't contact $pduname.infra: $!\n";
    $self->{'Ucam::Infra::PDU::snmp'} = $snmp;
    return $self;
}

sub snmp {
    my $self = shift;
    return $self->{'Ucam::Infra::PDU::snmp'};
}

sub rrdfile {
    my $self = shift;
    return "$rrddir/$self.rrd";
}

sub outlet {
    my $self = shift;
    my($outlet) = @_;
    return Ucam::Infra::PDU::Outlet->retrieve(pduname => $self,
					      outlet => $outlet);
}

1;
