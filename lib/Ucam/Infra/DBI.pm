package Ucam::Infra::DBI;

use base 'Class::DBI';

__PACKAGE__->connection("dbi:SQLite:dbname=/var/lib/infra-db/infra-db", "", "",
			{ RaiseError => 1 });

1;

