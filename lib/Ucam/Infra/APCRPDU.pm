package Ucam::Infra::APCRPDU;

use warnings;
use strict;

use base qw(Ucam::Infra::PDU);

use SNMP;

sub probe {
    my $class = shift;
    my($sysoid) = @_;
    # Somehow between RPDU versions 3.7.3 and 3.9.2, APC managed to duplicate part of this
    # OID path.
    return $sysoid eq '.1.3.6.1.4.1.318.1.3.4.5' ||
	   $sysoid eq '.1.3.6.1.4.1.318.1.3.4.5.1.3.4.5';
}

sub construct {
    my $proto = shift;
    SNMP::loadModules('PowerNet-MIB');
    return $proto->SUPER::construct(@_);
}

sub getstatus {
    my ($self, $outlet) = @_;
    my $val = $self->snmp->get("rPDUOutletStatusOutletState.$outlet");
    $val .= $self->snmp->get("rPDUOutletStatusCommandPending.$outlet") eq
	'outletStatusCommandPending' ? '*' : '';
    return $val;
}

sub getvers {
    my ($self) = @_;
    return $self->snmp->get("sPDUIdentFirmwareRev.0");
}

sub getmodel {
    my ($self) = @_;
    return $self->snmp->get("sPDUIdentModelNumber.0");
}

sub power_state {
    my ($self, $outlet) = @_;
    my $val = $self->snmp->get("rPDUOutletStatusOutletState.$outlet");
    return undef unless defined $val;
    return $val eq 'outletStatusOn';
}

sub on {
    my ($self, $outlet) = @_;
    $self->snmp->set("rPDUOutletControlOutletCommand.$outlet",
		       'immediateOn');
    if ($self->snmp->{ErrorNum}) {
	warn("SNMP request failed: ", $self->snmp->{ErrorStr}, "\n");
    }
}

sub off {
    my ($self, $outlet) = @_;
    $self->snmp->set("rPDUOutletControlOutletCommand.$outlet",
		       'immediateOff');
    if ($self->snmp->{ErrorNum}) {
	warn("SNMP request failed: ", $self->snmp->{ErrorStr}, "\n");
    }
}

sub reboot {
    my ($self, $outlet) = @_;
    $self->snmp->set("rPDUOutletControlOutletCommand.$outlet",
		       'immediateReboot');
    if ($self->snmp->{ErrorNum}) {
	warn("SNMP request failed: ", $self->snmp->{ErrorStr}, "\n");
    }
}

sub outletname {
    my $self = shift;
    my $outlet = shift;
    my $name;
    if (@_) {
	($name) = @_;
	$name = "Outlet $outlet" unless defined $name;
	while ($self->snmp->get("rPDUOutletConfigOutletName.$outlet") ne
	       $name) {
	    $self->snmp->set("rPDUOutletConfigOutletName.$outlet", $name);
	}
    } else {
	$name = $self->snmp->get("rPDUOutletConfigOutletName.$outlet");
    }
    return $name
}

sub getcurrent {
    my $self = shift;
    my $outlet = shift;
    return undef if defined $outlet;
    my $deciampere = $self->snmp->get("rPDULoadStatusLoad.1");
    return $deciampere / 10;
}

1;
