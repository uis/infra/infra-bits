package Ucam::Infra::PDU::Any;

use warnings;
use strict;

use base qw(Ucam::Infra::PDU);

use SNMP;
use Ucam::Infra::APCRPDU;
use Ucam::Infra::DominionPX;

sub construct {
    my $proto = shift;
    my($data) = @_;
    my $pduname = $data->{pduname};

    my $snmp = new SNMP::Session(DestHost => "$pduname.infra", Version => 1,
				 Community => "private", UseEnums => 1)
	or die "Can't contact $pduname.infra: $!\n";

    SNMP::loadModules('SNMPv2-MIB');
    my $sysoid = $snmp->get("sysObjectID.0");

    if (defined $sysoid) {
	for my $class (qw(Ucam::Infra::APCRPDU Ucam::Infra::DominionPX)) {
	    $class->probe($sysoid) && return $class->construct(@_);
	}
    }
    # Something went wrong.  Don't die, but return a useless PDU object.
    $snmp->{ErrorStr} eq '' or warn "$pduname: $snmp->{ErrorStr}\n";
    return $proto->SUPER::construct(@_);
}

sub AUTOLOAD {
    die "Unknown PDU type\n";
}

1;
