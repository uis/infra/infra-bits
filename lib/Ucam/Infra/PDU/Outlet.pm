package Ucam::Infra::PDU::Outlet;

use warnings;
use strict;

use Class::DBI::Column;

my $rrddir = "/var/lib/infra-stats";

 use base 'Ucam::Infra::DBI';
__PACKAGE__->table('outlets');
__PACKAGE__->columns(Primary =>
		     Class::DBI::Column->new(pduname => { accessor => 'pdu' }),
		     qw/outlet/);
__PACKAGE__->columns(Others => qw/boxname psuname/);
# Forcibly set "inflate" to ensure that the "construct" method gets called.
# See <http://wiki.class-dbi.com/wiki/Implementing_an_object_factory>
__PACKAGE__->has_a(pduname => 'Ucam::Infra::PDU', inflate => 'new');
__PACKAGE__->has_a(boxname => 'Ucam::Infra::Box');

sub box { goto &boxname };

sub new {
    my($class, $pdu, $outlet) = @_;

    return $class->retrieve(pduname => $pdu, outlet => $outlet);
}

# Find an outlet or outlets following the behaviour of the "pdu" command

__PACKAGE__->add_constructor(_find1 => q{boxname = ?1 OR pduname = ?1
					   ORDER BY pduname, outlet});
__PACKAGE__->add_constructor(_find2 => q{(boxname = ?1 AND psuname = ?2) OR
					     (pduname = ?1 AND outlet = ?2)
					   ORDER BY pduname, outlet});

sub find {
    my $proto = shift;
    my @outlets;

    foreach my $arg (@_) {
	my ($box, $psu) = split(/:/, $arg);

	push @outlets, defined($psu) ?
	    $proto->_find2($box, $psu) : $proto->_find1($box);
    }
    return @outlets;
}

sub getstatus {
    my $self = shift;
    return $self->pdu->getstatus($self->outlet);
}

sub getcurrent {
    my $self = shift;
    return $self->pdu->getoutletcurrent($self->outlet);
}

sub power_state {
    my $self = shift;
    return $self->pdu->power_state($self->outlet);
}
    

sub label {
    my $self = shift;
    if (defined $self->boxname) {
	if (defined $self->psuname) {
	    return $self->boxname . ':' . $self->psuname;
	}
	return $self->boxname;
    }
    return undef;
}

sub on {
    my $self = shift;
    $self->pdu->on($self->outlet);
}

sub off {
    my $self = shift;
    $self->pdu->off($self->outlet);
}

sub reboot {
    my $self = shift;
    $self->pdu->reboot($self->outlet);
}    

sub rrdfile {
    my $self = shift;
    if ($self->can('getcurrent')) {
	return sprintf("$rrddir/%s/%d.rrd", $self->pdu, $self->outlet);
    }
    return undef;
}

sub can {
    my $self = shift;
    my($method) = @_;

    if (ref $self && $method eq 'getcurrent') {
	return $self->pdu->can('getoutletcurrent');
    }
    return $self->SUPER::can(@_);
}

sub stringify_self {
    my $self = shift;
    return join ':', $self->pdu, $self->outlet;
}

1;
