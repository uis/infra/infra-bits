package Ucam::Infra::Box;

use warnings;
use strict;

use base 'Ucam::Infra::DBI';

__PACKAGE__->table('boxes');
__PACKAGE__->columns(Essential => qw/boxname/);
__PACKAGE__->columns(Other => qw/sysadmins console/);
__PACKAGE__->has_many(outlets => 'Ucam::Infra::PDU::Outlet', 'boxname');

use Net::LDAP;
use Net::LDAP::Util qw/escape_filter_value/;

my $basedn = 'ou=groups, o=University of Cambridge,dc=cam,dc=ac,dc=uk';

my $ldap;

our $binddn ||= undef;
our $password ||= undef;

sub _ldap_init {
    unless (defined($ldap)) {
	my $l = Net::LDAP->new('ldaps://ldap.lookup.cam.ac.uk/') or die $@;
	my $msg;
	if (defined $binddn) {
	    $msg = $l->bind($binddn, password => $password);
	} else {
	    $msg = $l->bind();
	}
	die $msg->error_text if $msg->is_error;
	$ldap = $l;
    }
}

sub sysadmin_is ($$) {
    my $self = shift;
    my ($username) = @_;

  search:
    {
	_ldap_init;
	my $groupid = $self->sysadmins;
	return 0 unless defined $groupid;
	my $msg = $ldap->search(base => $basedn,
				filter =>
				sprintf('(&(groupid=%s)(|(alluid=%s)(uid=%s)))',
					escape_filter_value($groupid),
					escape_filter_value($username),
					escape_filter_value($username)),
				attrs => ['1.1']);
	if ($msg->is_error) {
	    if ($msg->error_name eq 'LDAP_SERVER_DOWN' ||
		$msg->error_name eq 'LDAP_OPERATIONS_ERROR') {
		undef $ldap;
		redo search;
	    }
	    die $msg->error_text;
	}
	return $msg->count;
    }
}

1;

