Name: ucs-infra-bits
Summary: Bits of code for the UCS infrastructure network
License: none
Group: local
Source: %{name}-%{version}.tar.gz
Version: 1.47
Release: 1
Requires: perl(YAML::XS) perl(Class::DBI) perl(DBI) perl(DBD::SQLite) perl(Net::LDAP) perl(SNMP) perl(Text::CSV_XS) dhcp-server dominion-px-mib powernet-mib
BuildRoot: %{_tmppath}/%{name}-%{version}-build
BuildArch: noarch

%Description
This package contains various little scripts used to administer the
infrastructure network for the University of Cambridge Computing Service.

%Prep
%setup

%Build
perl Makefile.PL
make

%Install
rm -rf ${RPM_BUILD_ROOT}
make DESTDIR=${RPM_BUILD_ROOT} install_vendor
mkdir -p ${RPM_BUILD_ROOT}/etc/infra
mkdir -p ${RPM_BUILD_ROOT}/var/lib/infra-db
%perl_process_packlist

%Files
%defattr(-,root,root)
/etc/infra
/usr/bin/infra-gencollectd
/usr/bin/infra-gendhcp
/usr/bin/infra-genhosts
/usr/bin/infra-mkdb
/usr/bin/infra-updpdus
/usr/bin/pdu
/usr/share/man/man1/infra-mkdb.1.gz
/usr/share/man/man1/pdu.1.gz
%{perl_vendorlib}/Ucam/Infra/APCRPDU.pm
%{perl_vendorlib}/Ucam/Infra/Box.pm
%{perl_vendorlib}/Ucam/Infra/DBI.pm
%{perl_vendorlib}/Ucam/Infra/DominionPX.pm
%{perl_vendorlib}/Ucam/Infra/PDU.pm
%{perl_vendorlib}/Ucam/Infra/PDU/Any.pm
%{perl_vendorlib}/Ucam/Infra/PDU/Outlet.pm
%{perl_vendorarch}/auto/%{name}/.packlist
/var/adm/perl-modules/%{name}
%dir /var/lib/infra-db
%dir %attr(-,daemon,daemon) /var/lib/infra-stats
