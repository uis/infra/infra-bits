use ExtUtils::MakeMaker;

WriteMakefile
    ( NAME => 'ucs-infra-bits',
      EXE_FILES => [ qw(pdu infra-gendhcp infra-genhosts infra-gencollectd
			infra-mkdb
			infra-updpdus) ],
      PREREQ_PM => { YAML => 0,
		     DBI => 0,
		     DBD::SQLite => 0,
		     SNMP => 0 },
      VERSION => qx[sed -n 's/^Version: //p' ucs-infra-bits.spec]);
